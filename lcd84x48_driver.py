#
# This is the lcd driver for an 84 x 48 pixel "nokia" style lcd matrix mounted on a Pridopea expansion board
#
# Dependencies : RPI.GPIO
#              : time
#                
# Help document is in this directory under help.me
#

import time # delay functions
import RPi.GPIO as gpio # gpio functions

# pin assignment
GPIO_LCD_BL = 18
GPIO_LCD_DIN = 23
GPIO_LCD_SCLK = 24
GPIO_LCD_DC = 22
GPIO_LCD_RST = 17
GPIO_LCD_CS = 27

class clLCD:
    def __init__(self):
        gpio.setmode(gpio.BCM)
        time.sleep(1)
        gpio.setup(GPIO_LCD_BL, gpio.OUT)
        gpio.setup(GPIO_LCD_DIN, gpio.OUT)
        gpio.setup(GPIO_LCD_SCLK, gpio.OUT)
        gpio.setup(GPIO_LCD_DC, gpio.OUT)
        gpio.setup(GPIO_LCD_RST, gpio.OUT)
        gpio.setup(GPIO_LCD_CS, gpio.OUT)
        gpio.output(GPIO_LCD_CS, 1)
        gpio.output(GPIO_LCD_SCLK, 1)
        gpio.output(GPIO_LCD_RST, 0)
        time.sleep(0.01)
        gpio.output(GPIO_LCD_RST, 0)
        
    def __sendcmd__(self, cmd):
        # send command to the lcd
        CLKPERIOD = 0.001
        gpio.output(GPIO_LCD_CS, 0)
        gpio.output(GPIO_LCD_DC, 0)
        gpio.output(GPIO_LCD_CLK, 0)
        for bitcount in [7,6,5,4,3,2,1,0]:
            bit = ((cmd & (2**bitcount)) >> bitcount)
            gpio.output(GPIO_LCD_DIN, bit)
            time.sleep(CLKPERIOD / 2)
            gpio.output(GPIO_LCD_CLK, 1)
            time.sleep(CLKPERIOD / 2)
            gpio.output(GPIO_LCD_CLK, 0)
        time.sleep(CLKPERIOD / 2)
        gpio.output(GPIO_LCD_CS, 1)
    
    def __senddata__(self, data):
        # send data byte to the lcd
        CLKPERIOD = 0.001
        gpio.output(GPIO_LCD_CS, 0)
        gpio.output(GPIO_LCD_DC, 1)
        gpio.output(GPIO_LCD_CLK, 0)
        for bitcount in [7,6,5,4,3,2,1,0]:
            bit = ((cmd & (2**bitcount)) >> bitcount)
            gpio.output(GPIO_LCD_DIN, bit)
            time.sleep(CLKPERIOD / 2)
            gpio.output(GPIO_LCD_CLK, 1)
            time.sleep(CLKPERIOD / 2)
            gpio.output(GPIO_LCD_CLK, 0)
        time.sleep(CLKPERIOD / 2)
        gpio.output(GPIO_LCD_CS, 1)
        
    def backlight(self, cmd):
 #       '''
 #   Function : turn on or off the backlight
 #   Usage    : lcd.backlight(<param>)
 #   Params   : 'on' or 'off' to turn lcd on or off
 #       '''
        if cmd == 'on':
            gpio.output(GPIO_LCD_BL, 1)
        elif cmd == 'off':
            gpio.output(GPIO_LCD_BL, 0)
            
    def clear(self):
 #       '''
 #   Function : clear the lcd screen
 #   Usage    : lcd.clear()
 #   Params   : none
 #       '''    
        # clear screen
        pass
            
    def lprint(row, text):
 #               '''
 #   Function : send a line of text to the screen
 #   Usage    : lcd.lprint(<param1>, <param2>)
 #   Params   : param1 .... line to print to (string)
 #              param2 .... text (string)
 #       '''
        # put line
        pass
    
