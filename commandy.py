#
# commandy.py
#
# command interpreter for the UDP pan & tilt
#


from messaging import *
import time # delay functions
import lcd84x48_driver as lcdlib
import pan_tilt_driver as pntlib



def do(data, peripheral_list):

    ################# system commands ######################
    
    if data == 'close_server':
        msg = 'server closing'
    elif data == 'list_peripherals':
        msg = str(peripheral_list)
        
    ############### lcd display commands ###################
    
    elif data == 'lcd_setup':
        # look for an instance of an lcd
        if searchlist('lcd', peripheral_list):
            pass
        else:
            lcd = lcdlib.clLCD()
            peripheral_list.append(['lcd', lcd])
        msg = 'Setting up LCD'
    elif data == 'lcd_backlight_on':
        msg = getinstance('lcd', peripheral_list).backlight('on')
    elif data == 'lcd_backlight_off':
        msg = getinstance('lcd', peripheral_list).backlight('off')
    elif data.split()[0] == 'lcd_lprint':
        # command lprint "lprint <row> <text>"
        row = data.split()[1].strip()
        text = data.split()[2].strip()
        msg = getinstance('lcd', peripheral_list).lprint(row, text)
    elif data == 'lcd_clear':
        # look for an instance of an lcd
        if searchlist('lcd', peripheral_list):
            msg = getinstance('lcd', peripheral_list).clear()
        
    ############### pan and tilt commands ###################
    
    elif data == 'pnt_setup':
        if searchlist('pnt', peripheral_list):
            pass
        else:
            pnt = pntlib.clPNT()
            peripheral_list.append(['pnt', pnt])
        msg = 'Setting up pan and tilt'
    elif data == 'pnt_tilt_up':
        msg = getinstance('pnt', peripheral_list).move('up', 1)
    elif data == 'pnt_tilt_down':
        msg = getinstance('pnt', peripheral_list).move('down', 1)
    elif data == 'pnt_pan_right':
        msg = getinstance('pnt', peripheral_list).move('right', 1)
    elif data == 'pnt_pan_left':
        msg = getinstance('pnt', peripheral_list).move('left', 1)
    else:
        msg = 'command not recognized'
        # do nothing
        
    # print out the message 
    dbg_msg(msg)

    return peripheral_list
    
    
def searchlist(item, list):
    token = False
    # look for an instance of an <item>
    for entry in list:
        if entry[0] == item:
            token = True
    if token:
        return True
    else:
        return False
        
        
def getinstance(item,list):
    for entry in list:
        if entry[0] == item:
            return entry[1]
