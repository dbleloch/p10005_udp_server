#
# This is the pan & tilt unit driver for a 2-axis pan & tilt
#
# Dependencies : RPI.GPIO
#              : time
#                
# Help document is in this directory under help.me
#

import time # delay functions
import RPi.GPIO as gpio # gpio functions

# pin assignment
GPIO_UP = 18
GPIO_DOWN = 18
GPIO_LEFT = 18
GPIO_RIGHT = 18

class clPNT:
    def __init__(self):
        gpio.setmode(gpio.BCM)
        time.sleep(1)
        gpio.setup(GPIO_UP, gpio.OUT)
        gpio.setup(GPIO_DOWN, gpio.OUT)
        gpio.setup(GPIO_LEFT, gpio.OUT)
        gpio.setup(GPIO_RIGHT, gpio.OUT)

    def move(self, dir, movetime):
        # pick the direction
        if dir == 'right':
            gpio_pin = GPIO_RIGHT
        elif dir == 'left':
            gpio_pin = GPIO_LEFT
        elif dir == 'up':
            gpio_pin = GPIO_UP
        elif dir == 'down':
            gpio_pin = GPIO_DOWN
        # then move the p & t for the amount specified
        current_time = time.time()
        stop_time = current_time + movetime # hold the gpio line for n seconds then release
        gpio.output(gpio_pin, 1)
        while time.time() < stop_time:
            pass
        gpio.output(gpio_pin, 0)
            
