#
# This is the socket receiver function that runs on the Raspberry Pi to receive UDP packets
#
# Dependencies : RPI.GPIO
#                commandy.py
#                
# Help document is in this directory under help.me
#

import socket
import commandy as cmd

# list of pointers to peripherals attached to the server
old_peripheral_list = []

# UDP_IP = "192.168.1.82"
UDP_IP = "" # accept commands from all ip addresses
UDP_PORT = 5005 # this is an unreserved socket, but check for conflicts in with other proprietary code
RTN_PORT = 5007

# instantiate a socket for UDP and bind
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP over internet messaging
listen_address = (UDP_IP, UDP_PORT)
sock.bind(listen_address)

data = "no_received_message"

print "waiting for message..."

while data != "close_server":
    data, rxaddr = sock.recvfrom(1024)
    print "received message:", data
    print "from address", rxaddr
    peripheral_list = cmd.do(data, old_peripheral_list)
    old_peripheral_list = peripheral_list
    
# open a socket short term, specifically to report back to the client, then close it - UNTESTED, COMMENTED OUT
# retsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP over internet messaging
# return_address = (rxaddr, RTN_PORT)
# retsock.sendto(rc, return_address)
# retsock.close()
    
print "closing socket"
sock.close()
# sock = socket.INVALID_SOCKET
